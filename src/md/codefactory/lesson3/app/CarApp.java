package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Car;
import md.codefactory.lesson3.domain.Engine;

public class CarApp {
    public static void main(String[] args) {

        //create a new car
        Car c1 = new Car();
        //set the color
        c1.color="black";
        //set engine
        //first create engine
        Engine e = new Engine();
        e.capacity=6000;
        e.fuelType="gasoline";

        c1.engine = e;

        //show all attributes:
        // show car color
        System.out.println("car color is " + c1.color);
        //show the capacity of the engine for this car
        System.out.println("capacity is " + c1.engine.capacity);

    }
}
