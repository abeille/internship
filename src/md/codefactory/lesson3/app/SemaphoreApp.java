package md.codefactory.lesson3.app;

import md.codefactory.lesson3.domain.Semaphore;

public class SemaphoreApp {
    public static void main(String[] args) {
        //declare a new Semaphore Object
        Semaphore s1;
        //initialize s1 object
        s1 = new Semaphore();
        //show all properties for s1
        System.out.println(s1.color);
        System.out.println(s1.hasTimer);
        System.out.println("change s1 properties");

        //  x = 10;  -> set value for primitives
        // objectName.fieldName = <value>
        s1.color="red";
        System.out.println(s1.color);
        System.out.println(s1.hasTimer);


        Semaphore s2 = new Semaphore();
        //set color to green, and should have a timer
    }
}
