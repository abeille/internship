package md.codefactory.lesson6;

import md.codefactory.lesson6.utils.BankAccountUtil;
import md.codefactory.lesson6.utils.PinUtil;

import javax.swing.*;

public class ATMPtrogram {

    public static void main(String[] args) {
        System.out.println("Welcome to ATM");
        int numberOfTries = 0;
        String userInput;

        while (true) {
            userInput =
                    JOptionPane.showInputDialog("Enter your PIN");
            //convert String to INT
            int pin = Integer.parseInt(userInput);

            boolean isCorrectPin = PinUtil.validatePin(pin);
//            if(isCorrectPin==false)
            if (!isCorrectPin) {  // WRONG PIN, isCorrectPin is not true
                //how many tries ?
                if (numberOfTries == 2) {
                    showErrorMessage();
                    System.exit(-1);
                }
                numberOfTries++;
                JOptionPane.showMessageDialog(null,
                        "Sorry, wrong PIN\n Attempts left: " + (3 - numberOfTries));

            } else {
                //get out from while
                break;
            }
        }

        String[] options = new String[]{
                "100",
                "500",
                "1000",
                "2000",
                "other"
        };
        // here we have a correct PIN, ask for money
//            userInput
        Object userOption = JOptionPane.showInputDialog(null,
                "Select an ammount",
                "Amount Options",
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                null);
        userInput = (String) userOption;
        //validate for input
        if (userInput.equals("other")) {
            //show other input
            userInput = JOptionPane.showInputDialog("Enter custom amount");
        }
        // convert user input to int
        int ammount = Integer.parseInt(userInput);

        boolean isValidSum = BankAccountUtil.validateSum(ammount);
        if (isValidSum) {
            JOptionPane.showMessageDialog(null,
                    "Take your money, total " + ammount);
        } else {
            JOptionPane.showMessageDialog(null,
                    "Insufficient Funds");
        }

    }

    private static void showErrorMessage() {
        JOptionPane.showMessageDialog(null,
                "No more attempts left",
                "Attention",
                JOptionPane.ERROR_MESSAGE);
    }

}
