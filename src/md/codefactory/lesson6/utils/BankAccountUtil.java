package md.codefactory.lesson6.utils;

public class BankAccountUtil {

    //user amount here
    private static final int TOTAL_AMMOUNT = 1000;

    public static boolean validateSum(int userSum) {
        return userSum <= TOTAL_AMMOUNT;
    }

}
