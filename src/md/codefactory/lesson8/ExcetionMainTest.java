package md.codefactory.lesson8;

import md.codefactory.lesson8.dto.UserDataDto;
import md.codefactory.lesson8.exceptions.UserNotFoundException;
import md.codefactory.lesson8.service.UserService;

import javax.swing.*;
import java.io.IOException;

public class ExcetionMainTest {

    public static void main(String[] args) {

        UserService userService = new UserService();

        String userInput = JOptionPane
                .showInputDialog("Enter user ID please");
        long userId = Long.parseLong(userInput);

        try {
            UserDataDto useData = userService.getFullData(userId);
            displayUserData(useData);
        } catch (IOException e) {
            String errorMessage = "Sorry, file cannot be opened\n" +
                    "Contact administrator and show this message:\n"
                    + e.getMessage();
            showErrorMessage(errorMessage);
        } catch (UserNotFoundException e) {
            String errorMessage = e.getMessage();
            showErrorMessage(errorMessage);
        } catch (Exception e){
            String message = "An unexpected exception occurred";
            showErrorMessage(message);
        }

    }

    private static void showErrorMessage(String errorMessage) {
        JOptionPane.showMessageDialog(null,
                errorMessage,
                "Error",
                JOptionPane.ERROR_MESSAGE);
    }

    private static void displayUserData(UserDataDto useData) {
        JOptionPane.showMessageDialog(null,
                useData.toString(),
                "User Data",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
