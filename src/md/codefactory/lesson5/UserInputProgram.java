package md.codefactory.lesson5;

import md.codefactory.lesson5.domain.Settings;
import md.codefactory.lesson5.domain.UserColor;

import javax.swing.*;

public class UserInputProgram {

    public static void main(String[] args) {

        String color;
        int iterations;

        //String[] options = new String[]{"\u001B[31m", "\u001B[32m"};
        UserColor[] options = new UserColor[]{
                //1
                new UserColor("\u001B[31m", "RED"),
                //2
                new UserColor("\u001B[32m", "GREEN")
        };

        Object userInput = JOptionPane.showInputDialog(null,
                "Select a color",
                "Color chooser",
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options, options[0]);
        //cast userInput to string
        // color = (UserColor) userInput;

        UserColor selectedOption = (UserColor) userInput;
        color = selectedOption.getColorCode();

        String numberInput =
                JOptionPane.showInputDialog("How many iterations");
        iterations = Integer.parseInt(numberInput);

        Settings settings = SettingUtil
                .storeUserSettings(color, iterations);
        // UtilMethods.sayHello(settings);
        String text =
                JOptionPane.showInputDialog("Please insert your text");
        UtilMethods.sayHello(settings, text);
    }

}
