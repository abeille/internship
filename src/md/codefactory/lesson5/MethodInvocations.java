package md.codefactory.lesson5;

public class MethodInvocations {
    public static void main(String[] args) {
        UtilMethods.sayHello();
        UtilMethods.sayHello("Denis");
        UtilMethods.sayHello("John", 2);
    }
}
