package md.codefactory.lesson5;

public class DateUtilTest {
    public static void main(String[] args) {
        //invoke directly
        System.out.println(DateUtil.getCurrentDate());
        //store in a variable the result of the
        //method
        int todayDate = DateUtil.getCurrentDate();
        System.out.println("today is "+ todayDate);

         final String ANSI_RED = "\u001B[31m";
        final String ANSI_RESET = "\u001B[0m";
        System.out.println(ANSI_RED + "This text is red!" + ANSI_RESET);
    }
}
