package md.codefactory.lesson5;

import md.codefactory.lesson5.domain.Settings;

public class SettingUtil {

    public static Settings storeUserSettings(String color,
                                             int iterations) {
        Settings settings = new Settings();
        settings.setColor(color);
        settings.setIterations(iterations);
        return settings;
    }

}
