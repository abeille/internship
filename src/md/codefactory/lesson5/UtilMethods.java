package md.codefactory.lesson5;

import md.codefactory.lesson5.domain.Settings;

public class UtilMethods {

    public static void sayHello() {
        System.out.println("Hello");
    }

    public static void sayHello(String name) {
        System.out.println("Hello " + name);
    }

    public static void sayHello(String name, int iterations) {
        for (int i = 0; i < iterations; i++) {
            sayHello(name);
        }
    }

    public static void main(String[] args) {
        sayHello();
    }

    public static void sayHello(Settings settings) {
        for (int i = 0; i < settings.getIterations(); i++) {
            System.out.println(settings.getColor() + " hello");
        }
    }

    public static void sayHello(Settings settings, String text) {
        for (int i = 0; i < settings.getIterations(); i++) {
            System.out.println(settings.getColor() + text);
        }
    }

}
