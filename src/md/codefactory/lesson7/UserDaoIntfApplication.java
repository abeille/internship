package md.codefactory.lesson7;

import md.codefactory.lesson7.dao.UserDaoIntf;
import md.codefactory.lesson7.dao.impl.CSVUserDaoImpl;
import md.codefactory.lesson7.dao.impl.InmemoryUserDaoImpl;
import md.codefactory.lesson7.domain.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class UserDaoIntfApplication {
    public static void main(String[] args) throws IOException {

//        InmemoryUserDaoImpl userDao = new InmemoryUserDaoImpl();
        UserDaoIntf userDaoIntf = new InmemoryUserDaoImpl();

        User u1 = new User(1L,"John","Doe");
        User u2 = new User(2L,"Michel","Douglas");

        userDaoIntf.save(u1);
        userDaoIntf.save(u2);

        //check users
        List<User> users = userDaoIntf.findAll();
        System.out.println(users.get(0).getId()==1L);
        System.out.println(users.get(1).getId()==2L);

        userDaoIntf = new CSVUserDaoImpl();

        userDaoIntf.save(u1);
        System.out.println("******");

        userDaoIntf.save(u2);
    }
}
